var MPEG_TYPES = {},
    Box = function(name, extended){
      return function(parser, stream, box){
        box.name = name;
        extended(parser, stream, box);
      };
    },
    FullBox = function(name, extended){
      return Box(name, function(parser, stream, box){
        box.version = stream.readU8();
        box.flags = stream.readU24();
        extended(parser, stream, box);
      });
    },
    MPEG_BOX_CONTAINER = function MPEG_BOX_CONTAINER(name){
      return function(parser, stream, box){
        box.name = name;
        parser.readRemainingBoxes(stream, box);
      };
    };
MPEG_TYPES.ftyp = Box("File Type Box", function ftyp(parser, stream, box){
  var i, len = ((box.extendedSize || box.size) - 16) / 4;
  box.majorBrand = stream.readChars(4);
  box.minorVersion = stream.readU32();
  box.compatibleBrands = [];
  for (i = 0; i < len; i++) {
    box.compatibleBrands.push(stream.readChars(4));
  }
});
MPEG_TYPES.moov = Box("Movie Box", function moov(parser, stream, box){
  parser.readRemainingBoxes(stream, box);
});
MPEG_TYPES.mdat = Box("Media Data Box", function mdat(parser, stream, box){
  var i, len = (box.extendedSize || box.size) - 16;
  box.data = [];
  for (i = 0; i < len; i++) {
    box.data.push(stream.readU8());
  }
});

MPEG_TYPES.mvhd = FullBox("Movie Header Box", function mvhd(parser, stream, box){
  if (box.version === 1) {
    box.creationTime = stream.readU64();
    box.modificationTime = stream.readU64();
    box.timeScale = stream.readU64();
    box.duration = stream.readU64();
  } else {
    box.creationTime = stream.readU32();
    box.modificationTime = stream.readU32();
    box.timeScale = stream.readU32();
    box.duration = stream.readU32();
  }
  
  box.rate = stream.readFP16();
  box.volume = stream.readFP8();
  stream.skip(10); // Reserved
  box.matrix = stream.readU32Array(9);
  stream.skip(24); // Pre Defined
  box.nextTrackId = stream.readU32();
  stream.skip(parser.getRemainingBytes(stream, box));
});

MPEG_TYPES.trak = Box("Track Box", function trak(parser, stream, box){
  parser.readRemainingBoxes(stream, box);
});

MPEG_TYPES.tkhd = FullBox("Track Header Box", function tkhd(parser, stream, box){
  if (box.version === 1) {
    box.creationTime = stream.readU64();
    box.modificationTime = stream.readU64();
    box.trackId = stream.readU32();
    stream.skip(4); // Reserved unsigned int 32
    box.duration = stream.readU64();
  } else {
    box.creationTime = stream.readU32();
    box.modificationTime = stream.readU32();
    box.trackId = stream.readU32();
    stream.skip(4); // Reserved unsigned int 32
    box.duration = stream.readU32();
  }
  stream.skip(8); // Reserved
  box.layer = stream.readU16();
  box.alternateGroup = stream.readU16();
  box.volume = stream.readFP8();
  stream.skip(2); // Reserved unsigned int 16
  box.matrix = stream.readU32Array(9);
  box.width = stream.readFP16();
  box.height = stream.readFP16();
});

MPEG_TYPES.tref = Box("Track Reference Box", function tref(parser, stream, box){
  parser.readRemainingBoxes(stream, box);
});

MPEG_TYPES.hint = Box("Track Reference Hint Box", function hint(parser, stream, box){
  box.track_IDs = [];
  var i, len = ((box.extendedSize || box.size) - 8) / 4;
  for (i = 0; i < len; i++) {
    box.track_IDs.push(stream.readU32());
  }
});
MPEG_TYPES.cdsc = Box("Track Reference Description Box", function cdsc(parser, stream, box){
  box.track_IDs = [];
  var i, len = ((box.extendedSize || box.size) - 8) / 4;
  for (i = 0; i < len; i++) {
    box.track_IDs.push(stream.readU32());
  }
});

MPEG_TYPES.mdia = Box("Media Box", function mdia(parser, stream, box){
  parser.readRemainingBoxes(stream, box);
});

MPEG_TYPES.mdhd = FullBox("Media Header Box", function mdhd(parser, stream, box){
  if (box.version === 1) {
    box.creationTime = stream.readU64();
    box.modificationTime = stream.readU64();
    box.timescale = stream.readU32();
    box.duration = stream.readU64();
  } else {
    box.creationTime = stream.readU32();
    box.modificationTime = stream.readU32();
    box.timescale = stream.readU32();
    box.duration = stream.readU32();
  }
  stream.skip(1); // Pad
  box.language = stream.readISO639();
  stream.skip(2); // Pre defined
});

MPEG_TYPES.hdlr = FullBox("Handler Box", function hdlr(parser, stream, box){
  stream.skip(4); // Pre defined
  box.handlerType = stream.readU32();
  stream.skip(12); // Reserved
  box.name = stream.readNullUTF8();
});

MPEG_TYPES.minf = Box("Media Information Box", function minf(parser, stream, box){
  parser.readRemainingBoxes(stream, box);
});

MPEG_TYPES.vmhd = FullBox("Video Media Header Box", function vmhd(parser, stream, box){
  box.graphicsMode = stream.readU16();
  box.opcolor = stream.readU16Array();
});

MPEG_TYPES.smhd = FullBox("Sound Media Header Box", function smhd(parser, stream, box){
  box.balance = stream.readU16();
  stream.skip(2); // Reserved
});

MPEG_TYPES.hmhd = FullBox("Hint Media Header Box", function hmhd(parser, stream, box){
  box.maxPDUsize = stream.readU16();
  box.avgPDUsize = stream.readU16();
  box.maxBitrate = stream.readU32();
  box.avgBitrate = stream.readU32();
  stream.skip(4); // Reserved
});

MPEG_TYPES.nmhd = FullBox("Null Media Header Box", function nmhd(parser, stream, box){
});

MPEG_TYPES.dinf = Box("Data Information Box", function dinf(parser, stream, box){
  parser.readRemainingBoxes(stream, box);
});

MPEG_TYPES.url = FullBox("Data Entry URL Box", function url(parser, stream, box){
  box.location = stream.readNullUTF8();
});

MPEG_TYPES["urn "] = FullBox("Data Entry Urn Box", function urn(parser, stream, box){
  box.name = stream.readNullUTF8();
  box.location = stream.readNullUTF8();
});

MPEG_TYPES.dref = FullBox("Data Reference Box", function dref(parser, stream, box){
  var i, a;
  box.entryCount = stream.readU32();
  for (i = 0; i < box.entryCount; i++) {
    a = readBox(stream);
    box[a.type] = a;
  }
});

MPEG_TYPES.stbl = Box("Sample Table Box", function stbl(parser, stream, box){
  parser.readRemainingBoxes(stream, box);
});

MPEG_TYPES.stts = FullBox("Time To Sample Box", function stts(parser, stream, box){
  var i;
  box.entryCount = stream.readU32();
  box.entries = [];
  for (i = 0; i < box.entryCount; i++) {
    box.entries.push({
      sampleCount: stream.readU32(),
      sampleDelta: stream.readU32()
    });
  }
});

MPEG_TYPES.ctts = FullBox("Composition Offset Box", function ctts(parser, stream, box){
  var i;
  box.entryCount = stream.readU32();
  box.entries = [];
  for (i = 0; i < box.entryCount; i++) {
    box.entries.push({
      sampleCount: stream.readU32(),
      sampleOffset: stream.readU32()
    });
  }
});


MPEG_TYPES.mvex = Box("Movie Extends Box", function mvex(parser, stream, box){
  parser.readRemainingBoxes(stream, box);
});

MPEG_TYPES.moof = Box("Movie Fragment Box", function moof(parser, stream, box){
  parser.readRemainingBoxes(stream, box);
});

MPEG_TYPES.mfhd = FullBox("Movie Fragment Header Box", function mfhd(parser, stream, box){
  box.sequenceNumber = stream.readU32();
});

MPEG_TYPES.traf = Box("Track Fragment Box", function traf(parser, stream, box){
  parser.readRemainingBoxes(stream, box);
});

MPEG_TYPES.tfhd = FullBox("Track Fragment Header Box", function tfhd(parser, stream, box){
  box.trackId = stream.readU32();
  box.baseDataOffset = stream.readU64();
  box.sampleDescriptionIndex = stream.readU32();
  box.defaultSampleDuration = stream.readU32();
  box.defaultSampleSize = stream.readU32();
  box.defaultSampleFlags = stream.readU32();
});

MPEG_TYPES.sidx = FullBox("Segment Index Box", function sidx(parser, stream, box){
  var i, tmp, a;
  box.referenceId = stream.readU32();
  box.timeScale = stream.readU32();
  if (box.version === 1) {
    box.earliestPresentationTime = stream.readU64();
    box.firstOffset = stream.readU64();
  } else {
    box.earliestPresentationTime = stream.readU32();
    box.firstOffset = stream.readU32();
  }
  stream.skip(2); // Reserved
  box.referenceCount = stream.readU16();
  box.references = [];
  for (i = 0; i < box.referenceCount; i++) {
    a = {};
    tmp = stream.readU32();
    a.referenceType = (tmp & 0x80000000) >> 24;
    a.referenceSize = tmp & 0x7fffffff;
    a.subsegmentDuration = stream.readU32();
    tmp = stream.readU32();
    a.startsWithSAP = (tmp & 0x80000000) >> 24;
    a.SAPType = (tmp & 0x70000000) >> 24;
    a.SAPDeltaTime = tmp & 0xfffffff;
    box.references.push(a);
  }
});

MPEG_TYPES.ssix = FullBox("Subsegment Index Box", function ssix(parser, stream, box){
  var i, j, a;
  box.subsegmentCount = stream.readU32();
  box.subsegments = [];
  for (i = 0; i < box.subsegmentCount; i++) {
    a = {};
    a.rangeCount = stream.readU32();
    a.ranges = [];
    for (j = 0; j < a.rangeCount; j++) {
      a.ranges.push({
        level: stream.readU8(),
        rangeSize: stream.readU24()
      });
    }
    
    box.subsegments.push(a);
  }
});
MPEG_TYPES.prft = FullBox("Producer Reference Time Box", function prft(parser, stream, box){
  box.referenceTrackId = stream.readU32();
  box.ntpTimestamp = stream.readU64();
  if (version === 0) {
    box.mediaTime = stream.readU32();
  } else {
    box.mediaTime = stream.readU64();
  }
});

MPEG_TYPES.saiz = FullBox("Sample Auxiliary Information Sizes Box", function saiz(parser, stream, box){
  var i;
  if (box.flags & 1) {
    box.auxInfoType = stream.readU32();
    box.auxInfoTypeParameter = stream.readU32();
  }
  box.defaultSampleInfoSize = stream.readU8();
  box.sampleCount = stream.readU32();
  if (box.defaultSampleInfoSize === 0) {
    box.sampleInfoSize = [];
    for (i = 0; i < box.sampleCount; i++) {
      box.sampleInfoSize.push(stream.readU8());
    }
  }
});
MPEG_TYPES.saio = FullBox("Sample Auxiliary Information Offsets Box", function saio(parser, stream, box){
  var i;
  if (box.flags & 1) {
    box.auxInfoType = stream.readU32();
    box.auxInfoTypeParameter = stream.readU32();
  }
  box.entryCount = stream.readU32();
  box.entries = [];
  if (box.version === 1) {
    for (i = 0; i < box.entryCount; i++) {
      box.entries.push(stream.readU64());
    }
  } else {
    for (i = 0; i < box.entryCount; i++) {
      box.entries.push(stream.readU32());
    }
  }
});
MPEG_TYPES.trun = FullBox("Track Run Box", function trun(parser, stream, box){
  var i;
  box.sampleCount = stream.readU32();
  box.dataOffset = stream.readS32();
  box.firstSampleFlags = stream.readU32();
  box.samples = [];
  for (i = 0; i < box.sampleCount; i++) {
    box.samples.push({
      sampleDuration: stream.readU32(),
      sampleSize: stream.readU32(),
      sampleFlags: stream.readU32(),
      sampleCompositionTimeOffset: (box.version === 1 ? stream.readU32() : stream.readS32())
    });
  }
});
MPEG_TYPES.tfdt = FullBox("Track Fragment Base Media Decode Time Box", function tfdt(parser, stream, box){
  if (box.version === 1) {
    box.baseMediaDecodeTime = stream.readU64();
  } else {
    box.baseMediaDecodeTime = stream.readU32();
  }
});
MPEG_TYPES.leva = FullBox("Level Assignment Box", function leva(parser, stream, box){
  var i, tmp, level;
  box.levelCount = stream.readU8();
  box.levels = [];
  for (i = 0; i < box.levelCount; i++) {
    level.trackId = stream.readU32();
    tmp = stream.readU8();
    level.paddingFlags = (tmp >> 7) & 1;
    level.assignmentType = tmp & ~256;
    if (level.assignmentType === 0) {
      level.groupingType = stream.readU32();
    } else if (level.assignmentType === 1) {
      level.groupingType = stream.readU32();
      level.groupingTypeParameter = stream.readU32();
    } else if (level.assignmentType === 2) {
    } else if (level.assignmentType === 3) {
    } else if (level.assignmentType === 4) {
      level.subTrackId = stream.readU32();
    }
    box.levels.push(level);
  }
});