function MPEGParser(stream) {
  this.stream = stream;
}
MPEGParser.prototype.getRemainingBytes = function getRemainingBytes(stream, box) {
  var a = (box.extendedSize || box.size) - (stream.position - box.offset);
  if (a < 0) {
    console.warn("getRemainingBytes() returned a negative value", stream.position, (box.extendedSize || box.size));
    return 0;
  }
  return a;
};
MPEGParser.prototype.skipRemainingBytes = function skipRemainingBytes(stream, box) {
  stream.skip(this.getRemainingBytes(stream, box));
};
MPEGParser.prototype.readRemainingBoxes = function readRemainingBoxes(stream, box) {
  var len = this.getRemainingBytes(stream, box);
      subStream = stream.sub(stream.getPosition(), len);
  console.log("Reading Boxes in substream");
  this.readBoxes(subStream, box);
  console.log("Read all the boxes");
  stream.skip(len);
};

MPEGParser.prototype.read = function read(){
  var file = {};
  this.readBoxes(this.stream, file);
  return file;
};
MPEGParser.prototype.calls = 0;
MPEGParser.prototype.readBoxes = function readBoxes(stream, parent) {
  var child;
  while (!stream.isAtEnd() && this.calls < 100) {
    this.calls++;
    try {
      child = this.readBox(stream);
      if (child === null) continue;
      if (child.type.length !== 4) continue;
      if (child.type in parent) {
        parent[child.type] = [parent[child.type]];
        parent[child.type].push(child);
      } else {
        parent[child.type] = child;
      }
    } catch (e) {
      console.error(e, parent);
    }
  }
  if (this.calls >= 100) console.log("Force ending loop!");
};

MPEGParser.prototype.readBox = function readBox(stream) {
  /**
   * Standardized header for all box types
   **/
  function readHeader() {
    box.size = stream.readU32();
    box.type = stream.readChars(4);
    if (box.size === 1) {
      box.extendedSize = stream.readU64();
    } else if (box.size === 0) {
      
    }
    if (((box.extendedSize || box.size) < 8 && (box.extendedSize || box.size) > 1) || (box.extendedSize || box.size) < 0) throw "Size is not valid!";
  }
  
  var box = { offset: stream.position };
  readHeader();
  console.log(box);
  
  if (box.type in MPEG_TYPES) {
    MPEG_TYPES[box.type](this, stream, box);
  } else {
    console.warn("Box Type " + box.type + " support not implemented!", box);
    this.skipRemainingBytes(stream, box);
    box = null;
  }
  return box;
};