function DataStream(byteArray, offset, length) {
  this.byteArray = new Uint8Array(byteArray);
  this.offset = offset || 0;
  this.length = length || this.byteArray.length;
  this.byteLength = (this.offset + length) || this.byteArray.length;
  this.position = 0;
}
DataStream.prototype.getPosition = function getPosition(increment) {
  var pos = this.offset + this.position;
  if (typeof increment !== "number") increment = 0;
  this.position += increment;
  return pos;
};

DataStream.prototype.readUnsigned = function readUnsigned(len){
  if (len < 1) throw new Error("Invalid length!");
  var a = this.byteArray[this.getPosition()], i;
  for (i = 1; i < len; i++) {
    a |= this.byteArray[this.getPosition() + i] << i*8;
  }
  this.position += len;
  return a;
};

DataStream.prototype.readChar = function readChar() {
  return String.fromCharCode(this.byteArray[this.getPosition(1)]);
};

DataStream.prototype.readChars = function readChar(len) {
  var a = "", i;
  for (i = 0; i < len; i++) {
    a += this.readChar();
  }
  return a;
};

DataStream.prototype.readS8 = function readS8(){
  return this.readU8() << 24 >> 24;
};
DataStream.prototype.readU8 = function readU8(){
  return this.byteArray[this.getPosition(1)];
};
DataStream.prototype.readS16 = function readS16(){
  return this.readU16() << 16 >> 16;
};
DataStream.prototype.readU16 = function readU16(){
  var a = this.byteArray[this.getPosition()] << 8 | this.byteArray[this.getPosition() + 1];
  this.position += 2;
  return a;
};
DataStream.prototype.readS24 = function readS24(){
  return this.readU24() << 8 >> 8;
};
DataStream.prototype.readU24 = function readU24(){
  var a = this.byteArray[this.getPosition()] << 16 | this.byteArray[this.getPosition() + 1] << 8 | this.byteArray[this.getPosition() + 2];
  this.position += 3;
  return a;
};
DataStream.prototype.readS32 = function readS32(){
  return this.byteArray[this.getPosition() + 0] << 24 | this.byteArray[this.getPosition() + 1] << 16 | this.byteArray[this.getPosition() + 2] << 8 | this.byteArray[this.getPosition() + 3]
};
DataStream.prototype.readU32 = function readU32(){
  var a = this.byteArray[this.getPosition()] << 24 | this.byteArray[this.getPosition() + 1] << 16 | this.byteArray[this.getPosition() + 2] << 8 | this.byteArray[this.getPosition() + 3];
  this.position += 4;
  return a;
};
DataStream.prototype.readU40 = function readU40(){
  var a = this.byteArray[this.getPosition()] << 32 | this.byteArray[this.getPosition() + 1] << 24 | this.byteArray[this.getPosition() + 2] << 16 | this.byteArray[this.getPosition() + 3] << 8 | this.byteArray[this.getPosition() + 4];
  this.position += 5;
  return a;
};
DataStream.prototype.readU48 = function readU48(){
  var a = this.byteArray[this.getPosition()] << 40 | this.byteArray[this.getPosition() + 1] << 32 | this.byteArray[this.getPosition() + 2] << 24 | this.byteArray[this.getPosition() + 3] << 16 | this.byteArray[this.getPosition() + 4] << 8 | this.byteArray[this.getPosition() + 5];
  this.getPosition() += 6;
  return a;
};
DataStream.prototype.readU56 = function readU56(){
  var a = this.byteArray[this.getPosition()] << 48 | this.byteArray[this.getPosition() + 1] << 40 | this.byteArray[this.getPosition() + 2] << 32 | this.byteArray[this.getPosition() + 3] << 24 | this.byteArray[this.getPosition() + 4] << 16 | this.byteArray[this.getPosition() + 5] << 8 | this.byteArray[this.getPosition() + 6];
  this.position += 7;
  return a;
};
DataStream.prototype.readU64 = function readU64(){
  var a = this.byteArray[this.getPosition()] << 56 | this.byteArray[this.getPosition() + 1] << 48 | this.byteArray[this.getPosition() + 2] << 40 | this.byteArray[this.getPosition() + 3] << 32 | this.byteArray[this.getPosition() + 4] << 24 | this.byteArray[this.getPosition() + 5] << 16 | this.byteArray[this.getPosition() + 6] << 8 | this.byteArray[this.getPosition() + 7];
  this.position += 8;
  return a;
};

DataStream.prototype.seek = function seek(pos){
  var npos = Math.max(0, Math.min(this.byteLength, pos));
  this.position = (isNaN(npos) || !isFinite(npos)) ? 0 : npos;
};
DataStream.prototype.skip = function skip(len){
  if (isNaN(len)) len = 0;
  console.log("Skipping from " + this.getPosition() + " to " + (this.getPosition() + len) + " (" + len + ")");
  this.position += len;
};

DataStream.prototype.isAtEnd = function isAtEnd(){
  console.log(this.getPosition(), this.byteLength);
  return (this.getPosition() >= this.byteLength)
};

DataStream.prototype.sub = function sub(offset, length) {
  console.log("Creating a sub stream at " + offset + " with a length of " + length);
  return new DataStream(this.byteArray.buffer, offset + this.offset, length);
};

DataStream.prototype.readFP8 = function readFP8(){
  return this.readS16() / 256;
};
DataStream.prototype.readFP16 = function readFP16(){
  return this.readS32() / 65536;
};

DataStream.prototype.readU32Array = function readU32Array(rows, cols, names) {
  var i, j, row, array;
  cols = cols || 1;
  if (this.pos > this.end - (rows * cols) * 4)
    return null;
  if (cols === 1) {
    array = new Uint32Array(rows);
    for (i = 0; i < rows; i++) {
      row = null;
      if (names) {
        row = {};
        for (j = 0; j < cols; j++) {
          row[names[j]] = this.readU32();
        }
      } else {
        row = new Uint32Array(cols);
        for (j = 0; j < cols; j++) {
          row[j] = this.readU32();
        }
      }
    }
    array[i] = row;
  }
  return array;
};

DataStream.prototype.readISO639 = function readISO639(){
  var bits = this.readU16(), res = "", i, c;
  for (i = 0; i < 3; i++) {
    c = (bits >>> (2 - i) * 5) & 0x1f;
    res += String.fromCharCode(c + 0x60);
  }
  return res;
};

DataStream.prototype.readNullUTF8 = function readNullUTF8() {
  var c, t = "";
  while ((c = this.readU8()) !== 0x00) {
    t += String.fromCharCode(c);
  }
  return t;
};

DataStream.prototype.readUTF8 = function readUTF8(length) {
  var res = "", i;
  for (i = 0; i < length; i++) {
    res += String.fromCharCode(this.readU8());
  }
  return res;
};

DataStream.prototype.readPString = function readPString(max) {
  var len = this.readU8(), res;
  assert(len <= max);
  res = this.readUTF8(len);
  this.reserved(max - len - 1, 0);
  return res;
};

DataStream.prototype.reserved = function reserved(length, value) {
  var i;
  for (i = 0; i < length; i++) {
    assert(this.readU8() === value);
  }
};